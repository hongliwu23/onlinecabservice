## README ##

### Project Overview ###
This is the repository for the SWEN90007 project of 2017 Semester 2.

+ **Team number**: 25.
+ **Team members**: Hongli Wu & Jiawen Huang

####Main features of the project:####
> + **Feature 1**: managing booking orders for passengers
> + **Feature 2**: managing business orders for employee by business accounts.

####Feature implementation log:####
> + Feature 1 has been implemented by 11 Sep 2017.
> + Feature 2 has been implemented by 8 Oct 2017.




